Please write a class in the language of your choice that contains the following two public methods:

1. aboveBelow
accepts two arguments
a collection of integers (the list)
an integer (the comparison value)
returns a hash/object/map/etc. with the keys "above" and "below" with the corresponding count of integers from the list that are above or below the comparison value

Example usage:
input: [1,2,3,4,5,6], 3
output: { "above": 3, "below": 2 }

2. stringRotation
accepts two arguments
a string (the original string)
a positive integer (the rotation amount)
returns a new string, rotating the characters in the original string to the right by the rotation amount and have the overflow appear at the beginning

Example usage:
input: "MyString", 2
output: "ngMyStri"

```{python}

class RTS(): 
  
    def aboveBelow(self, list, number): 
        above_list = [x for x in list if x > number]
        below_list = [x for x in list if x < number]
        
        output_dictionary = {
          'above': len(above_list),
          'below': len(below_list)
        }
        
        return output_dictionary
  
    def stringRotation(self, string, n_rotation):
    
        ## get minimum n_rotations that has the same effect as the original n_rotation value;
        ## e.g. rotating a 8-character string by 18 will have the same effect as rotating the string by 2;
        ## this step makes sure that n_rotation value is always smaller than the length of the string;
        n_rotation = n_rotation % len(string)
    
        ## short version; this works
        #return string[-n_rotation:] + string[:-n_rotation]
      
        
        ## LONG VERSION (human friendly version)
        
        ## if there is no rotation to perform    
        if n_rotation == 0:
          
          ## save the original string
          rotated_string = string
        
        ## if we are pushing string from front to back
        elif n_rotation > 0:
          
          ## grab the back portion of the string and attach to the front (back portion goes to the front to form a new string)
          rotated_string = string[-n_rotation:] + string[:-n_rotation]  # back_portion + front_portion
          
        ## if we are pushing string from back to front
        elif n_rotation < 0:
          
          ## grab the front portion of the string and attach to the back (front portion goes to the back to form a new string)
          rotated_string = string[abs(n_rotation):] + string[:abs(n_rotation)]  # back_portion + front_portion 
    
        ## return rotated string      
        return rotated_string

    
    
## usage
rts = RTS()
rts.aboveBelow([1, 2, 3, 4, 5, 6], 3)

rts.stringRotation("MyString", 2)
rts.stringRotation("MyString", -2)
rts.stringRotation("MyString", 10)
rts.stringRotation("MyString", -10)
```